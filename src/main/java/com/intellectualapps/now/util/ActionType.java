/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.now.util;

public enum ActionType {
        
    TOKEN_CREATED("BILLING_AGREEMENT_TOKEN_CREATED"), ID_CREATED("BILLING_AGREEMENT_ID_CREATED"), CANCEL("BILLING_AGREEMENT_ID_CANCELED");
    
    String description;

    ActionType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

}

