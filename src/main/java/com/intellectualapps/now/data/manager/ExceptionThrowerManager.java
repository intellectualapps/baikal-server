/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.intellectualapps.now.data.manager;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import com.intellectualapps.now.util.exception.GeneralAppException;

/**
 *
 * @author buls
 */

@Stateless
public class ExceptionThrowerManager implements ExceptionThrowerManagerLocal {
    
    
    private final String CATEGORY_NOT_FOUND_ERROR = "Category not found";
    private final String CATEGORY_NOT_FOUND_ERROR_DETAILS = "The caegory with the supplied id does not exist";
    
    private final String INCOMPLETE_DATA_ERROR = "Incomplete data";
    private final String INCOMPLETE_DATA_ERROR_DETAILS = "Incomplete has been provided";
    
    private final String USER_CATEGORY_ALREADY_EXIST_ERROR = "User Category exists";
    private final String USER_CATEGORY_ALREADY_EXIST_ERROR_DETAILS = "User Category exists";
    
    private final String USER_CATEGORY_EXIST_ERROR = "User Category does not exists";
    private final String USER_CATEGORY_EXIST_ERROR_DETAILS = "User Category does not exists";    
    
    private final String ZAKAT_GOAL_ALREADY_EXIST_ERROR = "Zakat goal already exist.";
    private final String ZAKAT_GOAL_ALREADY_EXIST_ERROR_DETAILS = "User zakat goal already exist for this user";
    
    private final String ZAKAT_GOAL_DOES_NOT_EXIST_ERROR = "Zakat goal does not exist.";
    private final String ZAKAT_GOAL_DOES_NOT_EXIST_ERROR_DETAILS = "Zakat goal does not exists for this user";
    
    private final String INVALID_TOKEN_ERROR = "Invalid or expired token supplied";
    private final String INVALID_TOKEN_ERROR_DETAILS = "The auth token supplied is invalid or expired"; 
    
    private final String INVALID_DATE_FORMAT_ERROR = "Invalid date format";
    private final String INVALID_DATE_FORMAT_ERROR_DETAILS = "An invalid date format entered. Date should be in format yyyy-MM-dd";
    
    private final String USER_PAYMENT_OPTION_ALREADY_EXIST_ERROR = "Payment option already exist";
    private final String USER_PAYMENT_OPTION_ALREADY_EXIST_ERROR_DETAILS = "This payment option already exist for this user";
    
    private final String USER_PAYMENT_OPTION_DOES_NOT_EXIST_ERROR = "Payment option does not exist";
    private final String USER_PAYMENT_OPTION_DOES_NOT_EXIST_ERROR_DETAILS = "This payment option does exist for this user";
    
    private final String INVALID_PAYMENT_TYPE_ERROR= "Payment type does not exist"; 
    private final String INVALID_PAYMENT_TYPE_ERROR_DETAILS = "The specified payment type does not exist"; 
    
    private final String INVALID_LAST_FOUR_DIGIT_ERROR = "Invalid last four digits";
    private final String INVALID_LAST_FOUR_DIGIT_ERROR_DETAILS = "Invalid last four digits";
    
    private final String INVALID_IS_PRIMARY_ERROR = "Invalid is primary value";
    private final String INVALID_IS_PRIMARY_ERROR_DETAILS = "Invalid is primary value. Booolean (0 or 1) value expected.";
   
    private final String INVALID_INTEGER_VALUE_ERROR = "Integer value expected";
    private final String INVALID_INTEGER_VALUE_ERROR_DETAILS = "Could not parse value supplied. Integer value expected";
    
    private final String INVALID_DOUBLE_VALUE_ERROR = "Double value expected";
    private final String INVALID_DOUBLE_VALUE_ERROR_DETAILS = "Could not parse value supplied. Double value expected";
    
    private final String INVALID_EMAIL_ADDRESS_ERROR = "Invalid email address";
    private final String INVALID_EMAIL_ADDRESS_ERROR_DETAILS = "Invalid email address provided";
    
    private final String INVALID_CHARITY_ID_ERROR = "Charity does not exist";
    private final String INVALID_CHARITY_ID_ERROR_DETAILS = "Charity does not exist";
    
    private final String USER_CHARITY_ALREADY_EXIST_ERROR = "User Charity already exist";
    private final String USER_CHARITY_ALREADY_EXIST_ERROR_DETAILS = "This charity already exist for this user";
    
    private final String SAME_USER_PROVIDED_ERROR = "Same username provided";
    private final String SAME_USER_PROVIDED_ERROR_DETAILS = "Same username provided for follower and followed";
    
    private final String FOLLOWER_DOES_NOT_EXIST_ERROR = "Follower does not exist";
    private final String FOLLOWER_DOES_NOT_EXIST_ERROR_DETAILS = "The follower supplied does not follow this user";
    
    private final String INVITATION_DOES_NOT_EXIST_ERROR = "Invitation does not exist";
    private final String INVITATION_DOES_NOT_EXIST_ERROR_DETAILS = "The Invitation with the supplied id could not be found";
    
    private final String INVITATION_ALREADY_EXISTS_ERROR = "Invitation already exists";
    private final String INVITATION_ALREADY_EXISTS_ERROR_DETAILS = "The Invitation with the supplied invitee email already exists";
    
    private final String INVALID_STATE_TYPE_ERROR= "State type does not exist or is not allowed"; 
    private final String INVALID_STATE_TYPE_ERROR_DETAILS = "The specified state type is invalid or is not allowed"; 
    
    private final String PAYPAL_REST_CALL_ERROR = "API call failed";
    private final String PAYPAL_REST_CALL_ERROR_DETAILS = "API call to PayPal failed";
    
    private final String ERROR = "An exception occured.";
    private final String ERROR_DETAILS = "Error details: ";
    
    private final String INVALID_USER_IN_TOKEN_ERROR= "Could not find valid username details in token. Please re-initiate the process."; 
    private final String INVALID_USER_IN_TOKEN_ERROR_DETAILS = "The auth token does not contain a valid username"; 
    
    @Override
    public void throwNullPreferenceAttributesException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INCOMPLETE_DATA_ERROR, INCOMPLETE_DATA_ERROR_DETAILS, link);
    }
        
    @Override
    public void throwCategoryDoesNotExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.NOT_FOUND.getStatusCode(),
                400, CATEGORY_NOT_FOUND_ERROR, CATEGORY_NOT_FOUND_ERROR_DETAILS,
                link);
    }
            
    @Override
    public void throwUserCategoryAlreadyExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, USER_CATEGORY_ALREADY_EXIST_ERROR, USER_CATEGORY_ALREADY_EXIST_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwUserCategoryDoesNotExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.NOT_FOUND.getStatusCode(),
                400, USER_CATEGORY_EXIST_ERROR, USER_CATEGORY_EXIST_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwZakatGoalAlreadyExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, ZAKAT_GOAL_ALREADY_EXIST_ERROR, ZAKAT_GOAL_ALREADY_EXIST_ERROR_DETAILS,
                link);
    }
            
    @Override
    public void throwZakatGoalDoesNotExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.NOT_FOUND.getStatusCode(),
                400, ZAKAT_GOAL_DOES_NOT_EXIST_ERROR, ZAKAT_GOAL_DOES_NOT_EXIST_ERROR_DETAILS,
                link);
    }
        
    @Override
    public void throwInvalidTokenException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_TOKEN_ERROR, INVALID_TOKEN_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidDateFormatException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.NOT_FOUND.getStatusCode(),
                    404, INVALID_DATE_FORMAT_ERROR, INVALID_DATE_FORMAT_ERROR_DETAILS, link);
    }
    
    @Override
    public void throwUserPaymentOptionAlreadyExist (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, USER_PAYMENT_OPTION_ALREADY_EXIST_ERROR, USER_PAYMENT_OPTION_ALREADY_EXIST_ERROR_DETAILS,
                link);
        
    }
    
    @Override
    public void throwUserPaymentOptionDoesNotExist (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.NOT_FOUND.getStatusCode(),
                400, USER_PAYMENT_OPTION_DOES_NOT_EXIST_ERROR, USER_PAYMENT_OPTION_DOES_NOT_EXIST_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidPaymentTypeException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_PAYMENT_TYPE_ERROR, INVALID_PAYMENT_TYPE_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidLastFourDigitException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_LAST_FOUR_DIGIT_ERROR, INVALID_LAST_FOUR_DIGIT_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidIsPrimaryValueException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_IS_PRIMARY_ERROR, INVALID_IS_PRIMARY_ERROR_DETAILS,
                link);
    } 
    
    @Override
    public void throwInvalidIntegerAttributeException (String link) throws GeneralAppException{
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_INTEGER_VALUE_ERROR, INVALID_INTEGER_VALUE_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidDoubleAttributeException (String link) throws GeneralAppException{
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_DOUBLE_VALUE_ERROR, INVALID_DOUBLE_VALUE_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidEmailAddressException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_EMAIL_ADDRESS_ERROR, INVALID_EMAIL_ADDRESS_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwCharityDoesNotExistException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.NOT_FOUND.getStatusCode(),
                400, INVALID_CHARITY_ID_ERROR, INVALID_CHARITY_ID_ERROR_DETAILS,
                link);
    }
            
    @Override
    public void throwUserCharityAlreadyExistException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, USER_CHARITY_ALREADY_EXIST_ERROR, USER_CHARITY_ALREADY_EXIST_ERROR_DETAILS,
                link);
    }
                
    @Override
    public void throwSameUserProvidedException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, SAME_USER_PROVIDED_ERROR, SAME_USER_PROVIDED_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwFollowerDoesNotExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.NOT_FOUND.getStatusCode(),
                400, FOLLOWER_DOES_NOT_EXIST_ERROR, FOLLOWER_DOES_NOT_EXIST_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvitationDoesNotExistException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.NOT_FOUND.getStatusCode(),
                400, INVITATION_DOES_NOT_EXIST_ERROR, INVITATION_DOES_NOT_EXIST_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvitationAlreadyExistsException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.NOT_ACCEPTABLE.getStatusCode(),
                400, INVITATION_ALREADY_EXISTS_ERROR, INVITATION_ALREADY_EXISTS_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidStateTypeException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_STATE_TYPE_ERROR, INVALID_STATE_TYPE_ERROR_DETAILS,
                link);
    }   
    
    @Override
    public void throwPayPalRestCallException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, PAYPAL_REST_CALL_ERROR, PAYPAL_REST_CALL_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwGeneralException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, ERROR, ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidUserInTokenException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_USER_IN_TOKEN_ERROR, INVALID_USER_IN_TOKEN_ERROR_DETAILS,
                link);
    }
    
}

